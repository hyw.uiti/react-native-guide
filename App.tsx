import React, {useState, useEffect, useRef} from 'react';
import {SafeAreaView, FlatList, TextInput} from 'react-native';
import {Item} from './src/components/Item';
import {getAlbumData, Album} from './src/api/album';

const App = () => {
  const [wholeData, setWholeData] = useState<Album[]>();
  const [renderData, setRenderData] = useState<Album[]>();
  const offset = useRef<number>(0);

  useEffect(() => {
    (async () => {
      const albumData = await getAlbumData();
      setWholeData(albumData);
    })();
  }, []);

  useEffect(() => {
    setRenderData(wholeData?.slice(0, 10));
    offset.current = 10;
  }, [wholeData]);

  const renderMore = () => {
    if (renderData && wholeData) {
      setRenderData([
        ...renderData,
        ...wholeData?.slice(offset.current, offset.current + 10),
      ]);
    }
    offset.current = offset.current + 10;
  };

  const renderItem = ({item}: {item: Album}) => (
    <Item title={item.title} thumbnailUrl={item.thumbnailUrl} />
  );

  return (
    <SafeAreaView>
      <TextInput />
      <FlatList
        data={renderData}
        keyExtractor={item => `${item.id}`}
        renderItem={renderItem}
        onEndReached={renderMore}
        onEndReachedThreshold={0.3}
      />
    </SafeAreaView>
  );
};

export default App;
