import React from 'react';
import styled from 'styled-components/native';
import {View} from 'react-native';
import {Album} from '../api/album';

type ItemProps = {
  title: Album['title'];
  thumbnailUrl: Album['thumbnailUrl'];
};

export const Item = ({title, thumbnailUrl}: ItemProps) => (
  <View>
    <Title>{title.toLocaleUpperCase()}</Title>
    <Thumbnail source={{uri: thumbnailUrl}} />
  </View>
);

const Title = styled.Text`
  font-size: 30px;
`;

const Thumbnail = styled.Image`
  width: 500px;
  height: 200px;
`;
