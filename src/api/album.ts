import axios from 'axios';

export type Album = {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
};

export const getAlbumData = async (): Promise<Album[]> => {
  const {data} = await axios.get('https://jsonplaceholder.typicode.com/photos');
  return data;
};
